const csvtojson = require('csvtojson');
const path = require('path');
const fs = require('fs').promises;

const matchesCsvPath = path.join(__dirname, './matches.csv');
const deliveriesCsvPath = path.join(__dirname, './deliveries.csv');
const outputPath = path.join(__dirname, './');

csvtojson() // csvtojson library is used to convert csv data to json.
  .fromFile(matchesCsvPath)
  .then((matches) => {
    return matches = JSON.stringify(matches, null, 2); 
})
.then( (data) => {

  return fs.writeFile(
    path.join(outputPath, "matches.json"),data
  );

})
.then( () => {

return csvtojson().fromFile(deliveriesCsvPath)

})
.then((deliveries) => {

  return deliveries = JSON.stringify(deliveries, null, 2)

})
.then( (data) => {

  return fs.writeFile(
    path.join(outputPath, "deliveries.json"),data
  );

})
.then( () => {

  module.exports.matches = this.matches;
  module.exports.deliveries = this.deliveries;

})
.catch( (err) => {

  throw err;

})

// csvtojson()
//     .fromFile(deliveriesCsvPath)
//     .then((deliveries) => {
//         deliveries = JSON.stringify(deliveries, null, 2);

//         fs.writeFile(
//             path.join(outputPath, "deliveries.json"),
//             deliveries,
//             (error) => {
//               if (error) {
//                 console.log(error);
//               }
//             },
//         );  

//         module.exports.matches = this.matches;
//         module.exports.deliveries = this.deliveries;
        
//     })



