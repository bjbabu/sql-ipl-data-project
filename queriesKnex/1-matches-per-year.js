// 1. Number of matches played per year for all the years in IPL.
const db = require('../knexFile');

db.select('season')
.count('season', {as: 'count'})
.from('matches')
.groupBy('season')
.then( (rows) => {
    console.log(rows);
})
.catch( (err) => {

    throw err;

})
.finally(()=>{
    db.destroy();
})
