//2. Number of matches won per team per year in IPL.
const db = require('../knexFile');

db.select(
    'winner',
    'season'    
)
.count('winner', {as: 'count'})
.from('matches')
.whereNot({'winner': ''})
.groupBy(
    'winner',
    'season'
)
.orderBy('winner')
.then( (rows) => {
    console.log(rows);
})
.catch((err)=>{
    throw err;
})
.finally(()=>{
    db.destroy();
})
