// 9.Find the bowler with the best economy in super overs
const db = require('../knexFile');

db
.select(
	'bowler',
	'conceded_runs',
	'total_balls',
	'economy'
)
.from(db
		.select(
			'bowler',
			'conceded_runs',
			'total_balls',
			db.raw(`(conceded_runs / (total_balls / 6)) AS economy`),
			db.raw(`RANK() OVER (ORDER BY (conceded_runs / (total_balls / 6)) ASC) AS rnk`)
		)
		.from(db
			.select(
				'deliveries.bowler',
				db.raw(`SUM(deliveries.batsman_runs + deliveries.noball_runs + deliveries.wide_runs) AS conceded_runs`),
				db.raw(`
					SUM(
						CASE 
							WHEN deliveries.noball_runs = 0 AND deliveries.wide_runs = 0 
							THEN 1 
							ELSE 0 
						END) AS total_balls
				`)
			)
			.from('matches')
			.innerJoin('deliveries', 'matches.id', '=', 'deliveries.match_id')
			.where({'is_super_over': 1})
			.groupBy('deliveries.bowler')
			.as('subquery1')
		)
		.orderBy('economy')
		.as('subquery2')
)
.where({'rnk' : 1})
.then( (rows) => {
	console.log(rows);
})
.catch( (err) => {
	throw err;
})
.finally(() => {
	db.destroy();
})