// 4.Top 10 economical bowlers in the year 2015
const db = require('../knexFile');

db
.select(
	'bowler',
	'conceded_runs',
	'total_balls',
	db.raw(`(conceded_runs / (total_balls / 6)) AS economy`)
)
.from
(
	db
	.select(
		'deliveries.bowler',
		db.raw(`SUM(deliveries.batsman_runs + deliveries.noball_runs + deliveries.wide_runs) AS conceded_runs`),
		db.raw(`
			SUM(
				CASE 
					WHEN deliveries.noball_runs = 0 AND deliveries.wide_runs = 0 
					THEN 1 
					ELSE 0 
				END) AS total_balls
		`)
	)
	.from('matches')
	.innerJoin('deliveries', 'matches.id', '=', 'deliveries.match_id')
	.where({'matches.season': 2015})
	.groupBy('deliveries.bowler')
	.as('subquery')
)
.orderBy('economy')
.limit(10)
.then( (rows) => {
	console.log(rows);
})
.catch( (err) => {
	throw err;
})
.finally(() => {
	db.destroy();
})
