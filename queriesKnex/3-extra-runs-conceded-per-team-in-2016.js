// 3. Extra runs conceded per team in the year 2016
const db = require('../knexFile');

db
.select(
	'deliveries.bowling_team'
)
.sum('deliveries.extra_runs', {as: 'conceded_runs'})
.from('matches')
.innerJoin('deliveries', 'matches.id', '=', 'deliveries.match_id')
.where({'matches.season': 2016})
.groupBy('deliveries.bowling_team')
.then( (rows) => {

	console.log(rows);

})
.catch((err) => {
	throw err;
})
.finally(()=>{
	db.destroy();
})