// 8. Find the highest number of times one player has been dismissed by another player
const db = require('../knexFile');

db
.select(
	'bowler',
	'player_dismissed',
	'count'
)
.from(db
		.select(
			'bowler',
			'player_dismissed',
			db.raw(`RANK() OVER (ORDER BY COUNT(player_dismissed) DESC) AS rnk`)
		)
		.count('player_dismissed', {as: 'count'})
		.from('deliveries')
		.whereNot({'player_dismissed' : ''})
		.whereNotIn('dismissal_kind', ['run out', 'retired hurt'])
		.groupBy('bowler', 'player_dismissed')
		.as('subquery')
)
.where({'rnk' : 1})
.then( (rows) => {
	console.log(rows);
})
.catch( (err) => {
	throw err;
})
.finally( () => {
	db.destroy();
})