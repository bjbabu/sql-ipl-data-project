// 7. Find the strike rate of a batsman for each season
const db = require('../knexFile');

db
.select(
    'batsman',
    'season',
    db.raw(`( (total_runs * 100) / total_balls ) AS strike_rate`)
)
.from(db
        .select(
            'deliveries.batsman',
            'matches.season',
            db.raw(`SUM(deliveries.batsman_runs) AS total_runs`),
            db.raw(`SUM(CASE 
                            WHEN deliveries.noball_runs = 0 AND deliveries.wide_runs = 0 
                                THEN 1 
                                ELSE 0 
                        END) AS total_balls
                    `)
        )
        .from('deliveries')
        .innerJoin('matches', 'deliveries.match_id', '=', 'matches.id')
        .groupBy('deliveries.batsman', 'matches.season')
        .as('subquery1')
)
.then( (rows) => {
    console.log(rows);
})
.catch( (err) => {
    throw err;
})
.finally( () =>{
    db.destroy();
})