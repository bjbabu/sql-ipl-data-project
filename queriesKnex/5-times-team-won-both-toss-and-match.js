// 5. Find the number of times each team won the toss and also won the match
const db = require('../knexFile');

db
.select(
    'winner'
)
.count('winner', {as: 'count'})
.from('matches')
.where('winner', '=', db.raw('toss_winner'))
.groupBy('winner')
.then( (rows) => {
    console.log(rows);
})
.catch( (err) => {
    throw err;
})
.finally( () => {
    db.destroy();
})