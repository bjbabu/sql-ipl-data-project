// 6.Find a player who has won the highest number of Player of the Match awards for each season
const db = require('../knexFile');

db.select(
	'season',
	'player_of_match',
	'count'
)
.from(
	db
	.select(
		'season',
		'player_of_match',
		db.raw(`RANK() OVER (PARTITION BY season ORDER BY COUNT(player_of_match) DESC) AS rnk`)
	)
	.count('player_of_match', {as : 'count'})
	.from('matches')
	.groupBy('season', 'player_of_match')
	// .orderBy('season')
	.as('subquery')
)
.where({'rnk' : 1})
.then( (rows) => {
	console.log(rows);
})
.catch( (err) => {
	throw err;
})
.finally( () => {
	db.destroy();
})
