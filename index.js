const mysql = require('mysql2');
const matches = require('./data/matches.json');
const deliveries = require('./data/deliveries.json');

const db = mysql.createConnection({
    
    host: "localhost",
  
    user: "root",

    port: 3306,
  
    password: "Babu@299",

});

db.promise().query("CREATE DATABASE IF NOT EXISTS IPL")
.then(() => {

    return db.promise().query("USE IPL")

})
.then(()=>{
    console.log("Database Created");

    return db.promise().query(`
        CREATE TABLE IF NOT EXISTS matches(
            id INT AUTO_INCREMENT PRIMARY KEY,
            season YEAR(4) NOT NULL,
            city VARCHAR(255) NOT NULL,
            date DATE NOT NULL,
            team1 varchar(255) NOT NULL,
            team2 varchar(255) NOT NULL,
            toss_winner varchar(255) NOT NULL,
            toss_decision varchar(255) NOT NULL,
            result varchar(255) NOT NULL,
            dl_applied INT NOT NULL,
            winner varchar(255) NOT NULL,
            win_by_runs INT NOT NULL,
            win_by_wickets INT NOT NULL,
            player_of_match varchar(255) NOT NULL,
            venue varchar(255) NOT NULL,
            umpire1 varchar(255) NOT NULL,
            umpire2 varchar(255) NOT NULL,
            umpire3 varchar(255)
        )
    `)
})
.then(() => {

    console.log("Matches table created!!");

    return db.promise().query("SELECT * FROM IPL.matches")
    .then( ([rows]) => {

        if(rows.length === 0) {
           return insertIntoMatches(matches);
        }else{

            return Promise.resolve(rows);

        }

    })

})
.then( (data) => {
    console.log("Data inserted into mathes table!!");

    return db.promise().query(`
        CREATE TABLE IF NOT EXISTS deliveries(
            match_id INT NOT NULL,
            inning INT NOT NULL,
            batting_team VARCHAR(255) NOT NULL,
            bowling_team VARCHAR(255) NOT NULL,
            overs INT NOT NULL,
            ball INT NOT NULL,
            batsman VARCHAR(255) NOT NULL,
            non_striker VARCHAR(255) NOT NULL,
            bowler VARCHAR(255) NOT NULL,
            is_super_over boolean NOT NULL,
            wide_runs INT NOT NULL,
            bye_runs INT NOT NULL,
            legbye_runs INT NOT NULL,
            noball_runs INT NOT NULL,
            penalty_runs INT NOT NULL,
            batsman_runs INT NOT NULL,
            extra_runs INT NOT NULL,
            total_runs INT NOT NULL,
            player_dismissed VARCHAR(255),
            dismissal_kind VARCHAR(255),
            fielder VARCHAR(255),
            FOREIGN KEY (match_id) REFERENCES matches(id)
        )
    `)

})
.then(() => {
    console.log("Deliveries table created!!");

    return db.promise().query("SELECT * FROM IPL.deliveries")
    .then( ([rows]) => {

        if(rows.length === 0) {
            return insertIntoDeliveries(deliveries);
        }else{

            return Promise.resolve(rows);

        }

    })

})
.then( (data) => {

    console.log("Deliveries data inserted into the table!!!");

})
.catch((err) =>{
    throw err;
})
.then( () => db.end());



function insertIntoMatches(matches) {

    return new Promise( (resolve, reject) => {

        const queries = matches.map( (data) => {
        
            return db.promise().query(`
                INSERT IGNORE INTO matches
                (
                    season, city, date, team1, team2,
                    toss_winner, toss_decision, result,dl_applied,
                    winner,win_by_runs, win_by_wickets,player_of_match,
                    venue, umpire1, umpire2, umpire3
                )
                VALUES
                (
                    ${data.season}, "${data.city}", "${data.date}", "${data.team1}", "${data.team2}",
                    "${data.toss_winner}", "${data.toss_decision}", "${data.result}", ${parseInt(data.dl_applied)},
                    "${data.winner}", ${parseInt(data.win_by_runs)}, ${parseInt(data.win_by_wickets)}, "${data.player_of_match}",
                    "${data.venue}", "${data.umpire1}", "${data.umpire2}", "${data.umpire3}"
                )
            `)
    
        })

        Promise.all(queries)
        .then(results => {
            resolve(results);
        })
        .catch(err => {
            reject(err);
        })

    });

}

function insertIntoDeliveries(deliveries) {

    return new Promise( (resolve, reject) => {

        const queries = deliveries.map( (data) => {
        
            return db.promise().query(`
                INSERT IGNORE INTO deliveries
                VALUES
                (
                    ${data.match_id}, ${data.inning}, "${data.batting_team}", "${data.bowling_team}", ${data.over},
                    ${data.ball}, "${data.batsman}", "${data.non_striker}", "${data.bowler}", ${data.is_super_over},
                    ${data.wide_runs}, ${data.bye_runs}, ${data.legbye_runs}, ${data.noball_runs}, ${data.penalty_runs},
                    ${data.batsman_runs}, ${data.extra_runs}, ${data.total_runs}, "${data.player_dismissed}",
                    "${data.dismissal_kind}", "${data.fielder}"
                )
            `)
    
        })

        Promise.all(queries)
        .then(results => {
            resolve(results);
        })
        .catch(err => {
            reject(err);
        })

    });

}