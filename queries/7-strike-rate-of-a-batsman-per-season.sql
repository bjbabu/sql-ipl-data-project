-- 7. Find the strike rate of a batsman for each season

USE IPL;

-- SELECT * FROM deliveries;
-- SELECT * FROM matches;

SELECT 
    batsman, season, ((total_runs) * 100 / balls) AS strike_rate
FROM
    (SELECT 
        d.batsman,
            m.season,
            SUM(d.batsman_runs) AS total_runs,
            SUM(CASE
                WHEN d.noball_runs = 0 AND d.wide_runs = 0 THEN 1
                ELSE 0
            END) AS balls
    FROM
        deliveries AS d
    INNER JOIN matches AS m ON d.match_id = m.id
    GROUP BY m.season , d.batsman) AS subquery
ORDER BY batsman , season;