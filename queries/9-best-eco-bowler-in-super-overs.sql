-- 9.Find the bowler with the best economy in super overs
USE IPL;

-- SELECT * FROM deliveries;

SELECT bowler, total_runs, balls, economy
FROM
(
	SELECT bowler, total_runs, balls, (total_runs/((balls)/6)) AS economy,
			RANK() OVER (ORDER BY (total_runs/((balls)/6)) ) AS rnk
	FROM 
	(
			SELECT bowler, SUM(batsman_runs + noball_runs + wide_runs) AS total_runs,
					SUM(CASE 
							WHEN noball_runs = 0 AND wide_runs = 0 
								THEN 1 
								ELSE 0 
						END) AS balls
			FROM deliveries
				WHERE is_super_over = 1
				GROUP BY bowler
	) AS subquery1
)AS subquery2
WHERE rnk = 1;