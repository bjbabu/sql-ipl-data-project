-- Number of matches played per year for all the years in IPL.
USE IPL;
-- SELECT * FROM matches;

SELECT season, COUNT(season) AS count 
FROM IPL.matches
GROUP BY season
ORDER BY season ASC;

