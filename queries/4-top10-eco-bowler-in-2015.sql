-- 4.Top 10 economical bowlers in the year 2015
USE IPL;

-- SELECT * FROM matches;
-- SELECT * FROM deliveries;

SELECT bowler, conceded_runs, balls, ((conceded_runs) / (balls/6)) AS economy 
FROM (
        SELECT d.bowler AS bowler, 
		SUM(d.batsman_runs + d.noball_runs + d.wide_runs) AS conceded_runs,
		SUM(CASE 
				 WHEN d.noball_runs = 0 AND d.wide_runs = 0 THEN 1 
				 ELSE 0 
			  END) AS balls
		FROM IPL.deliveries AS d
		INNER JOIN IPL.matches AS m
					ON d.match_id = m.id
		WHERE m.season = 2015
		GROUP BY d.bowler
) AS result
ORDER BY economy ASC
LIMIT 10;

