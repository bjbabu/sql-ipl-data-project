-- 8. Find the highest number of times one player has been dismissed by another player

USE IPL;

-- SELECT * FROM deliveries;

SELECT bowler, player_dismissed, times_dismissed
FROM(
	SELECT bowler, player_dismissed, 
			COUNT(bowler) AS times_dismissed,
			RANK() OVER (ORDER BY COUNT(bowler) DESC) AS rnk
	FROM deliveries
			WHERE player_dismissed <> "" AND (dismissal_kind NOT IN ("run out", "retired hurt" ))
			GROUP BY bowler, player_dismissed
)AS subquery
WHERE rnk = 1;