-- 5. Find the number of times each team won the toss and also won the match
USE IPL;

-- SELECT * FROM matches;

SELECT winner, COUNT(winner)
FROM IPL.matches
WHERE winner = toss_winner
GROUP BY winner;