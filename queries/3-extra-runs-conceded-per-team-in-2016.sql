-- 3.Extra runs conceded per team in the year 2016
USE IPL;

-- SELECT * FROM matches;
-- SELECT * FROM deliveries;

SELECT d.bowling_team, SUM(d.extra_runs)
FROM deliveries AS d
INNER JOIN matches AS m
			ON (d.match_id = m.id)
WHERE m.season = 2016
GROUP BY d.bowling_team;