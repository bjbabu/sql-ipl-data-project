-- 2.Number of matches won per team per year in IPL.
USE IPL;

-- SELECT * FROM matches;


SELECT winner, season, COUNT(winner)
FROM IPL.matches
WHERE winner <> ""
GROUP BY season, winner
ORDER BY winner, season;
