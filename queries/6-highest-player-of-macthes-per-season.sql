-- 6.Find a player who has won the highest number of Player of the Match awards for each season
USE IPL;

-- SELECT * FROM matches;

SELECT season, player_of_match, cnt 
FROM(
		SELECT season, player_of_match, cnt,
		RANK() OVER (PARTITION BY season ORDER BY cnt DESC) AS rnk
		FROM(
				SELECT season, player_of_match, COUNT(player_of_match) AS cnt
				FROM IPL.matches
				GROUP BY season, player_of_match
			)AS subquery1
	)AS subquery2
WHERE rnk = 1;
